#What it does?

This framework is done to test this https://any-api.com/xkcd_com/xkcd_com/docs/API_Description

http://xkcd.com/info.0.json (current comic) -> returns the comic of the day

or:

http://xkcd.com/614/info.0.json (comic #614) -> returns the comic given by id

#How to install

Go to Gitlab and copy the link from Clone with Https In intellij press on Get from VCS and set the link and clone

#How to run

In terminal type mvn clean verify and press enter or right click on test.feature and click Run 'Feature:Test' (in Edit configuration make sure you have written only stepsDefinition in Glue)

#Generate reports

After you run the command mvn clean verify go into target/site/serenity and open file index.html in any browser

#How to write tests

In test/resources/test.feature you can add any new test